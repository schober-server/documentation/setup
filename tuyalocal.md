# TuyaLocal Setup for Tuya Lights

If you want to setup the tuya-local integration with HomeAssistant feel free to follow this guide.
It's based on the [official documentation](https://github.com/make-all/tuya-local/blob/7a53bc1c381830a90e1ff83f1c8a37f2e8af1e00/README.md), but in my opionion broken down to the most important steps.

## Prepare Tuya Cloud

- Install [com.tuya.smart](https://play.google.com/store/apps/details?id=com.tuya.smart) - needed to connect lights initialy with Tuya cloud
- Connect lights to Tuya Smart account, turn them of 3-5x times until they start blinking and then they should appear in the app
- Create Tuya Developer account [here](https://myaccount.tuya.com/account) - is not the same account as above
- Create project under `Cloud -> Development` ([link](https://platform.tuya.com/cloud))

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/01-open-developer-overview.png)
- Then link app with developer account under `<project name> -> Devices -> Link App Account`

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/02-link-accounts.png)
- Now your devices should appear under `<project name> -> Devices -> All Devices`

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/03-show-devices.png)
- Copy all `Device IDs`
- Navigate to `Cloud -> API Explorer` ([link](https://eu.platform.tuya.com/cloud/explorer))

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/04-api-explorer.png)
- Get the `local_key` via `Device Management -> Query Device Details in Bulk`

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/05-get-local_key.png)

## Setup with Home Assistant

- When setting up local-tuya in home assistant, make sure, that you Android Tuya Smart App is _not_ running and has _no_ connection to those devices. **But** make sure, that the devices still can reach the Tuya Cloud and are display as `Online` in the devices menu. Otherwise you could/will get weird error messages, like `Connection to device succeeded but no datapoints found, please try again. Create a new issue and include debug logs if problem persists.` - Make sure you temporarily disable PiHole + FW rules to drop traffic from Smart Home VLAN to the internet
- Because we already got the `local_key` from the cloud, we can skip the connection to the cloud via local-tuya

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/06-setup-ha-integration.png)
- Then click on `CONFIGURE` and add a new device. Use `...` to create a new device from scratch.

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/07-add-device.png)
- Enter details we got before

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/08-enter-credentials.png)
- Choose `light` as type

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/09-select-type.png)
- Enter values as follows to use the full feature set of at least `Manjusaka RGB / RGBCW Smart Light from Tuya`

| Name       | Which ID to pick | Value displayed |
|------------|------------------|-----------------|
| ID         | 20               | False           |
| Brightness | 22               | 1000            |
| Color Temp | 23               | 150             |
| Color Mode | 21               | white           |
| Color      | 24               | 000003e803e8    |
| Scene      | 26               | 0               |


![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/10-configure-01.png)

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/11-configure-02.png)
- Finally choose `light` again

![](https://gitlab.com/schober-server/documentation/images/-/raw/b5cc7eaa550fc8e82de24ad7783960a42040e207/home-assistant/tuya-smart-lights/12-final-select-type.png)
- Now you are done and can put your lights behind a FW, so they don't always talk home to the Tuya cloud
- 🎉✨

