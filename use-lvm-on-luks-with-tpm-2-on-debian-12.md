# LVM on LUKS with TPM 2 (+ btrfs) 🤯

## Install of Debian

When installing Debian 12 it's possible to select something like `LVM (encrypted)`.
This installs [LVM on LUKS](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS) and _not_ [LUKS on LVM](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LUKS_on_LVM).

Also Debian uses `ext4` by default, make sure you change it durring the install or you will have problems later on.

Finally keep in mind, that Debian 12 uses creates 2 additional partitions: `/boot` (`ext2`) and `/boot/efi` (`vfat`).
Both use by default around `512 MiB`. The rest will be used by one big LUKS partition.
So if you plan to install more OSes or multiple kernel you could tweak the size of those partitions now.


## Enable SecureBoot

Basically just follow [this documentation](https://wiki.debian.org/SecureBoot#Shim).

So generate your custom MOK: 

```sh
cd /var/lib/shim-signed/mok/
openssl req -nodes -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -days 36500 -subj "/CN=My Name/"
openssl x509 -inform der -in MOK.der -out MOK.pem
```

Enroll your key either with this command:

```sh
mokutil --import /var/lib/shim-signed/mok/MOK.der # prompts for one-time password
```

or just add it to your UEFI.


Test if key was enrolled:

```sh
mokutil --test-key /var/lib/shim-signed/mok/MOK.der
```

Finally sign your kernel:

```sh
VERSION="$(uname -r)"
SHORT_VERSION="$(uname -r | cut -d . -f 1-2)"
MODULES_DIR=/lib/modules/$VERSION
KBUILD_DIR=/usr/lib/linux-kbuild-$SHORT_VERSION

sbsign --key MOK.priv --cert MOK.pem "/boot/vmlinuz-$VERSION" --output "/boot/vmlinuz-$VERSION.tmp"
mv "/boot/vmlinuz-$VERSION.tmp" "/boot/vmlinuz-$VERSION"
```

## Enroll keys into TPM

Make sure you have enabled TPM in your UEFI and then use the [Arch Wiki documentation](https://wiki.archlinux.org/title/Systemd-cryptenroll#Trusted_Platform_Module) to enroll the keys.

TLDR:

```sh
systemd-cryptenroll --tpm2-device=auto /dev/sdX
```

## Automatic decrypting with dracut and TPM

Put this to your `dracut` config in e.g. `/etc/dracut.conf` or in a subdirectory if you prefer:

```
add_dracutmodules+=" tpm2-tss crypt "
```

## Setup GRUB

You also have to tell GRUB which LUKS volume you want to decrypt, do this with adding following line to `/etc/default/grub`:

```
GRUB_CMDLINE_LINUX="rd.luks.name=444e978a-ade0-4dfb-af21-1256f38bc3ff=mycryptroot"
```

## /etc/crypttab

Make that your `/etc/crypttab` is empty, otherwise you still could be prompted when logging in.
Also note, that if you have multiple LUKS volumes that you may have to disable other systemd services that try to mount those other volumes as well on boot.

```sh
systemctl status systemd-cryptsetup@myraidname.service
```

Done 🎉


## FAQ: My system runs instable and "forgets" the key to unlock the disks

For a temporary fix, just add the old keys back in:

```sh
systemd-cryptenroll --tpm2-device=auto /dev/sdX
```

If you get an error message with something like `DA lockout mode`, you have to unlock it first:

```sh
tpm2_dictionarylockout --clear-lockout
```

And if you want to increase the retries before this lockout happens use this to set it to the max amount:

```sh
tpm2_dictionarylockout --setup-parameters --max-tries=4294967295 --clear-lockout
```

